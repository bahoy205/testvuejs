// src/axios.js
import axios from 'axios';

// ตั้งค่า base URL สำหรับ Axios
axios.defaults.baseURL = 'http://localhost:3000/api/v1';

// ตั้งค่า header สำหรับการตรวจสอบสิทธิ์
axios.defaults.headers.common['Authorization'] = 'Bearer YOUR_ACCESS_TOKEN';

// ตั้งค่า timeout
axios.defaults.timeout = 10000; // 10 seconds

export default axios;